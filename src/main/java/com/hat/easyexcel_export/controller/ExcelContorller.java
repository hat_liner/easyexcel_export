package com.hat.easyexcel_export.controller;

import com.hat.easyexcel_export.service.ExcelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description  excel导出
 * @author HuangAnting
 * @date 2022/4/12 17:26
*/
@RestController
@RequestMapping("/excel")
@Slf4j
public class ExcelContorller {
    @Autowired
    private ExcelService excelService;
    @GetMapping(value = "exportExcel")
    public void  exportDoseValue(HttpServletResponse response ) {
        excelService.exportDoseValue(response);
    }
}
