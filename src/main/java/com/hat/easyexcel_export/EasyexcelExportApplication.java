package com.hat.easyexcel_export;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyexcelExportApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyexcelExportApplication.class, args);
    }

}
