package com.hat.easyexcel_export.service;

import javax.servlet.http.HttpServletResponse;

public interface ExcelService {
    /**
     * @Description:  导出
     * @Author Huanganting
     * @Date 2022/4/12 16:53
     * @param response:
     */
    void exportDoseValue(HttpServletResponse response);
}
