package com.hat.easyexcel_export.entity;

import lombok.Data;

@Data
public class MonPoint {
    /** 经度*/
    private Double lon;
    /** 纬度*/
    private Double lat;

}
