package com.hat.easyexcel_export.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.hat.easyexcel_export.excel.DateConverter;
import com.hat.easyexcel_export.excel.MonPointConverter;
import lombok.Data;

/**
 * @Description 导出实体类
 * @author HuangAnting
 * @date 2022/4/12 17:26
*/
@Data
public class Car {
    /** 名称*/
    @ExcelProperty(value = "名称",index = 1)
    private String name;
    /** 价格*/
    @ExcelProperty(value = "价格",index = 2)
    private Double price;
    /** 描述*/
    @ColumnWidth(15)
    @ExcelProperty(value = "描述",index = 3)
    private String description;
    /** 坐标*/
    @ExcelProperty(value = "坐标",converter = MonPointConverter.class,index = 4)
    private MonPoint monPoint;
    /** 日期*/
    @ExcelProperty(value = "日期",converter = DateConverter.class)
    private Long createDate;
}
