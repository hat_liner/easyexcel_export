package com.hat.easyexcel_export.excel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.alibaba.fastjson.JSONObject;
import com.hat.easyexcel_export.entity.MonPoint;


/**
 * @Description 将实体类转换成字符串类型
 * @author HuangAnting
 * @date 2022/4/12 17:28
*/
public class MonPointConverter implements Converter<MonPoint> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return MonPoint.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public WriteCellData<?> convertToExcelData(MonPoint value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
       return new WriteCellData<>(JSONObject.toJSONString(value));
    }

}