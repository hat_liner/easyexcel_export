package com.hat.easyexcel_export.excel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description excel日期转换类 即将Mongo中类型为long的时间戳转化为yyyy-MM-dd HH:mm:ss时间
 * @author HuangAnting
 * @date 2022/4/8 10:16
*/
public class DateConverter implements Converter<Long> {
    @Override
    public Class<?> supportJavaTypeKey() {
        //对象属性类型
        return Long.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        //CellData属性类型
        return CellDataTypeEnum.STRING;
    }
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Long> context) throws Exception {
        //对象属性转CellData
        Long cellValue = context.getValue();
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(cellValue));
        if (cellValue == null) {
            return new WriteCellData<>("");
        } else {
            return new WriteCellData<>( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(cellValue)));
        }
    }
}
